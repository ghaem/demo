<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DaraeesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DaraeesTable Test Case
 */
class DaraeesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DaraeesTable
     */
    public $Daraees;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.daraees',
        'app.users',
        'app.posts',
        'app.requests',
        'app.experts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Daraees') ? [] : ['className' => 'App\Model\Table\DaraeesTable'];
        $this->Daraees = TableRegistry::get('Daraees', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Daraees);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
