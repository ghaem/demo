<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DownloadTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DownloadTable Test Case
 */
class DownloadTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DownloadTable
     */
    public $Download;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.download'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Download') ? [] : ['className' => 'App\Model\Table\DownloadTable'];
        $this->Download = TableRegistry::get('Download', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Download);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
