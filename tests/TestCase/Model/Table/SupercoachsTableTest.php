<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SupercoachsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SupercoachsTable Test Case
 */
class SupercoachsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SupercoachsTable
     */
    public $Supercoachs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.supercoachs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Supercoachs') ? [] : ['className' => 'App\Model\Table\SupercoachsTable'];
        $this->Supercoachs = TableRegistry::get('Supercoachs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Supercoachs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
