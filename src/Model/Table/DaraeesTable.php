<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Daraees Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Daraee get($primaryKey, $options = [])
 * @method \App\Model\Entity\Daraee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Daraee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Daraee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Daraee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Daraee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Daraee findOrCreate($search, callable $callback = null, $options = [])
 */
class DaraeesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

       // $this->addBehavior('Timestamp');
        $this->setTable('daraees');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('number_daraee')
            ->requirePresence('number_daraee', 'create')
            ->notEmpty('number_daraee');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->allowEmpty('client');

        $validator
            ->integer('number_seryal')
            ->allowEmpty('number_seryal');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->requirePresence('departeman', 'create')
            ->notEmpty('departeman');

        $validator
            ->requirePresence('recever', 'create')
            ->notEmpty('recever');

        $validator
            ->requirePresence('maker', 'create')
            ->notEmpty('maker');

        $validator
            ->requirePresence('model', 'create')
            ->notEmpty('model');

        $validator
            ->requirePresence('adress_ip', 'create')
            ->notEmpty('adress_ip');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
