<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Experts Model
 *
 * @property \Cake\ORM\Association\HasMany $Requests
 *
 * @method \App\Model\Entity\Expert get($primaryKey, $options = [])
 * @method \App\Model\Entity\Expert newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Expert[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Expert|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Expert patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Expert[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Expert findOrCreate($search, callable $callback = null, $options = [])
 */
class ExpertsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('experts');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');

        $this->hasMany('Requests', [
            'foreignKey' => 'expert_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmpty('Id', 'create');

        $validator
            ->requirePresence('Name', 'create')
            ->notEmpty('Name');

        $validator
            ->requirePresence('Family', 'create')
            ->notEmpty('Family');

        $validator
            ->requirePresence('Email', 'create')
            ->notEmpty('Email');

        $validator
            ->requirePresence('Status', 'create')
            ->notEmpty('Status');

        $validator
            ->allowEmpty('Expertise');

        $validator
            ->allowEmpty('Education');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        return $validator;
    }
}
