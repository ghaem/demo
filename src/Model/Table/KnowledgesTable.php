<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Knowledges Model
 *
 * @method \App\Model\Entity\Knowledge get($primaryKey, $options = [])
 * @method \App\Model\Entity\Knowledge newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Knowledge[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Knowledge|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Knowledge patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Knowledge[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Knowledge findOrCreate($search, callable $callback = null, $options = [])
 */
class KnowledgesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('knowledges');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('number', 'create')
            ->notEmpty('number');

        $validator
            ->requirePresence('titel', 'create')
            ->notEmpty('titel');

        $validator
            ->requirePresence('Headlines', 'create')
            ->notEmpty('Headlines');

        $validator
            ->requirePresence('points', 'create')
            ->notEmpty('points');

        $validator
            ->integer('Number_people')
            ->requirePresence('Number_people', 'create')
            ->notEmpty('Number_people');

        $validator
            ->dateTime('Last_date')
            ->requirePresence('Last_date', 'create')
            ->notEmpty('Last_date');

        $validator
            ->requirePresence('writer', 'create')
            ->notEmpty('writer');

        $validator
            ->requirePresence('Show_user', 'create')
            ->notEmpty('Show_user');

        return $validator;
    }
}
