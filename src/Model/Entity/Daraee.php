<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Daraee Entity
 *
 * @property int $id
 * @property string $name
 * @property int $number_daraee
 * @property string $type
 * @property string $client
 * @property int $number_seryal
 * @property string $status
 * @property string $departeman
 * @property string $recever
 * @property string $maker
 * @property string $model
 * @property string $adress_ip
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 */
class Daraee extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
