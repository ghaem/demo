<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Request Entity
 *
 * @property int $Id
 * @property string $request
 * @property string $Type
 * @property \Cake\I18n\Time $Created
 * @property string $Status
 * @property string $Prority
 * @property \Cake\I18n\Time $Modified
 * @property int $user_id
 * @property int $expert_id
 * @property int $Numrequest
 * @property string $response
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Expert $expert
 */
class Request extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'Id' => false
    ];
}
