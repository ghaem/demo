<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppsController extends Controller
{
    public function initialize()
{
    $this->loadComponent('Flash');
    $this->loadComponent('Auth', [
    'authenticate' => [
    'Form' => [
    'fields' => [
    'username' => 'username',
    'password' => 'password'   
],
        'userModel' => 'Supercoachs',
//                    'passwordHasher' => [
//                        'className' => 'Fallback',
//                        'hashers' => [
//                            'Default',
//                            'Weak' => ['hashType' => 'sha256']
//                        ]
//                    ]
                ],
            ],
        
     'authError'    => 'اجازه دسترسی ندارید',
    'loginAction' => [
    'controller' => 'Supercoachs',
    'action' => 'addlogin',
     
],
      'redirectAction' =>[
                'controller' => 'Supercoachs',
                'action' => 'index'
            ],
            'storage' => ['className' => 'Session', 'key' => 'Auth.Supercoachs'],
             
    'unauthorizedRedirect' => $this->referer() // If unauthorized, return
//them to page they were just on
]);
    $this->Auth->allow(['display']);
  }
 


    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    
    }
  /*  public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(array('index','addUsr'));
        $this->Auth->authError='دسترسی به این قسمت امکان پذیر نیست';
        $this->Auth->loginAction = array('controller'=>'users','action'=>'login');
          $this->Auth->logoutRedirect= array('controller'=>'requests','action'=>'index');
    }
*/}


