<?php
namespace App\Controller;

use App\Controller\AppuController;

/**
 * Knowledges Controller
 *
 * @property \App\Model\Table\KnowledgesTable $Knowledges
 */
class KnowledgesController extends AppuController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $knowledges = $this->paginate($this->Knowledges);

        $this->set(compact('knowledges'));
        $this->set('_serialize', ['knowledges']);
    }

    /**
     * View method
     *
     * @param string|null $id Knowledge id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $knowledge = $this->Knowledges->get($id, [
            'contain' => []
        ]);

        $this->set('knowledge', $knowledge);
        $this->set('_serialize', ['knowledge']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $knowledge = $this->Knowledges->newEntity();
        if ($this->request->is('post')) {
            $knowledge = $this->Knowledges->patchEntity($knowledge, $this->request->getData());
            if ($this->Knowledges->save($knowledge)) {
                $this->Flash->success(__('The knowledge has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The knowledge could not be saved. Please, try again.'));
        }
        $this->set(compact('knowledge'));
        $this->set('_serialize', ['knowledge']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Knowledge id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $knowledge = $this->Knowledges->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $knowledge = $this->Knowledges->patchEntity($knowledge, $this->request->getData());
            if ($this->Knowledges->save($knowledge)) {
                $this->Flash->success(__('The knowledge has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The knowledge could not be saved. Please, try again.'));
        }
        $this->set(compact('knowledge'));
        $this->set('_serialize', ['knowledge']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Knowledge id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $knowledge = $this->Knowledges->get($id);
        if ($this->Knowledges->delete($knowledge)) {
            $this->Flash->success(__('The knowledge has been deleted.'));
        } else {
            $this->Flash->error(__('The knowledge could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
      public function search() {
     $search = $this->Knowledges->find('all');
           
            
         if ($this->request->is('post')) {
             $user= $this->request->getData('search');
            $search = $this->Knowledges->find('all',array('conditions'=>array('titel'=>$user),
           'field'=> array('id','number' ,'points '))); 
  
         
          
         }
           $this->paginate = [
            'contain' => []
        ];
         $daraees = $this->paginate($this->Knowledges);
         $this->set(compact('search'));
         $this->set('_serialize', ['daraees']);
}
}