<?php
namespace App\Controller;

use App\Controller\AppuController;

/**
 * Daraees Controller
 *
 * @property \App\Model\Table\DaraeesTable $Daraees
 */
class DaraeesController extends AppuController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $daraees = $this->paginate($this->Daraees);

        $this->set(compact('daraees'));
        $this->set('_serialize', ['daraees']);
    }

    /**
     * View method
     *
     * @param string|null $id Daraee id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $daraee = $this->Daraees->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('daraee', $daraee);
        $this->set('_serialize', ['daraee']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $daraee = $this->Daraees->newEntity();
        if ($this->request->is('post')) {
            $daraee = $this->Daraees->patchEntity($daraee, $this->request->getData());
            if ($this->Daraees->save($daraee)) {
                $this->Flash->success(__('The daraee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The daraee could not be saved. Please, try again.'));
        }
        $users = $this->Daraees->Users->find('list', ['limit' => 200]);
        $this->set(compact('daraee', 'users'));
        $this->set('_serialize', ['daraee']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Daraee id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $daraee = $this->Daraees->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $daraee = $this->Daraees->patchEntity($daraee, $this->request->getData());
            if ($this->Daraees->save($daraee)) {
                $this->Flash->success(__('The daraee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The daraee could not be saved. Please, try again.'));
        }
        $users = $this->Daraees->Users->find('list', ['limit' => 200]);
        $this->set(compact('daraee', 'users'));
        $this->set('_serialize', ['daraee']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Daraee id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $daraee = $this->Daraees->get($id);
        if ($this->Daraees->delete($daraee)) {
            $this->Flash->success(__('The daraee has been deleted.'));
        } else {
            $this->Flash->error(__('The daraee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    

    public function search() {
     $search = $this->Daraees->find('all');
           
            
         if ($this->request->is('post')) {
             $user= $this->request->getData('search');
            $search = $this->Daraees->find('all',array('conditions'=>array('name'=>$user),
           'field'=> array('id','number_daraee'
            ,'type'))); 
          
         }
           $this->paginate = [
            'contain' => ['Users']
        ];
         $daraees = $this->paginate($this->Daraees);
         $this->set(compact('search'));
         $this->set('_serialize', ['daraees']);
   
    }
    
        
    

}
