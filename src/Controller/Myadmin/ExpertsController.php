<?php
namespace App\Controller\Myadmin;

use App\Controller\AppeController;

/**
 * Experts Controller
 *
 * @property \App\Model\Table\ExpertsTable $Experts
 */
class ExpertsController extends AppeController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $experts = $this->paginate($this->Experts);

        $this->set(compact('experts'));
        $this->set('_serialize', ['experts']);
    }

    /**
     * View method
     *
     * @param string|null $id Expert id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $expert = $this->Experts->get($id, [
            'contain' => ['Requests']
        ]);

        $this->set('expert', $expert);
        $this->set('_serialize', ['expert']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $expert = $this->Experts->newEntity();
        if ($this->request->is('post')) {
            $expert = $this->Experts->patchEntity($expert, $this->request->getData());
            if ($this->Experts->save($expert)) {
                $this->Flash->success(__('The expert has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The expert could not be saved. Please, try again.'));
        }
        $this->set(compact('expert'));
        $this->set('_serialize', ['expert']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Expert id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $expert = $this->Experts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $expert = $this->Experts->patchEntity($expert, $this->request->getData());
            if ($this->Experts->save($expert)) {
                $this->Flash->success(__('The expert has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The expert could not be saved. Please, try again.'));
        }
        $this->set(compact('expert'));
        $this->set('_serialize', ['expert']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Expert id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $expert = $this->Experts->get($id);
        if ($this->Experts->delete($expert)) {
            $this->Flash->success(__('The expert has been deleted.'));
        } else {
            $this->Flash->error(__('The expert could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
  public function login()
   {
     if ($this->request->is('post')) {
     $expert = $this->Auth->identify();
     if ($expert) {
     $this->Auth->setUser($expert);
     //return $this->redirect($this->Auth->redirectUrl());
     return $this->redirect([
           'controller' => 'Experts',
            'action' => 'index']);
     }
     $this->Flash->error('رمز یا ایمیل صحیح نمی باشد');
    }
  }
  public function initialize()
   {
    parent::initialize();
      $this->Auth->allow(['logout']);
    }
    public function logout()
    {
       $this->Flash->success('شما خارج شدید');
       return $this->redirect($this->Auth->logout());
    }
}
