<?php



namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppeController extends Controller
{
    public function initialize()
{
    $this->loadComponent('Flash');
    $this->loadComponent('Auth', [
    'authenticate' => [
    'Form' => [
    'fields' => [
    'username' => 'Email',
    'password' => 'password'   
],
        'userModel' => 'Experts',
                    'passwordHasher' => [
                        'className' => 'Fallback',
                        'hashers' => [
                            'Default',
                            'Weak' => ['hashType' => 'sha256']
                        ]
                    ]
                ],
            ],
        
     'authError'    => 'اجازه دسترسی ندارید ',
    'loginAction' => [
    'controller' => 'Experts',
    'action' => 'login',
     
],
      'redirectAction' =>[
                'controller' => 'Experts',
                'action' => 'index'
            ],
            'storage' => ['className' => 'Session', 'key' => 'Auth.Experts'],
             
    'unauthorizedRedirect' => $this->referer() // If unauthorized, return
//them to page they were just on
]);
    $this->Auth->allow(['display']);
  }
 


    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    
    }

}



