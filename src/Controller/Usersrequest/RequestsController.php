<?php
namespace App\Controller\Usersrequest;

use App\Controller\AppuController;

/**
 * Requests Controller
 *
 * @property \App\Model\Table\RequestsTable $Requests
 */
class RequestsController extends AppuController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Experts']
        ];
        $requests = $this->paginate($this->Requests);

        $this->set(compact('requests'));
        $this->set('_serialize', ['requests']);
    }

    /**
     * View method
     *
     * @param string|null $id Request id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $request = $this->Requests->get($id, [
            'contain' => ['Users', 'Experts']
        ]);

        $this->set('request', $request);
        $this->set('_serialize', ['request']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $request = $this->Requests->newEntity();
        if ($this->request->is('post')) {
            $request = $this->Requests->patchEntity($request, $this->request->getData());
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('The request has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The request could not be saved. Please, try again.'));
        }
        $users = $this->Requests->Users->find('list', ['limit' => 200]);
        $experts = $this->Requests->Experts->find('list', ['limit' => 200]);
        $this->set(compact('request', 'users', 'experts'));
        $this->set('_serialize', ['request']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Request id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $request = $this->Requests->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $request = $this->Requests->patchEntity($request, $this->request->getData());
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('The request has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The request could not be saved. Please, try again.'));
        }
        $users = $this->Requests->Users->find('list', ['limit' => 200]);
        $experts = $this->Requests->Experts->find('list', ['limit' => 200]);
        $this->set(compact('request', 'users', 'experts'));
        $this->set('_serialize', ['request']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Request id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $request = $this->Requests->get($id);
        if ($this->Requests->delete($request)) {
            $this->Flash->success(__('The request has been deleted.'));
        } else {
            $this->Flash->error(__('The request could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
      public function request()
   {
         //$data=$this->request->getData();
         $request = $this->Requests->newEntity();
         if ($this->request->is('post')) {
          $data['user_id']=$this->Auth->user('id');
          $data['expert_id']=1;
          $data['request']=$this->request->getData('request');
           $data['response']='hhhhhhh';
          $data['Type']=$this->request->getData('Type');
          $data['Prority']=$this->request->getData('Prority');
          $data['Created']= \Cake\I18n\Time::now();
          //$data['MODI']= \Cake\I18n\Time::now();
          // $data['createdsabt']= \Cake\I18n\Time::now();Modified Createdrequest
          $data['Modified']= \Cake\I18n\Time::now();
          $rand= rand(1, 1000);
          $data['Numrequest']=$rand;
          $data['Status']='در حال بررسی';
           
          //$error='hhhhhhhhhhhhh';
          $request = $this->Requests->patchEntity($request, $data);
         if ($this->Requests->save($request)) {
            $this->Flash->success(__('The request has been saved.'));

                return $this->redirect(['action' => 'index']);
                  $num = $request-> Numrequest;
        
            }
           
             $this->Flash->show_messages_error(($request->getError()));
        }
   }
   public function readrequest(){
     $this->set('title_for_layout','list requests')  ;
        
     $req=$this->Requests->find('all',array('conditions'=>array('user_id'=>$this->Auth->user('id')),'field'=>array('request','status','id')))->all();
     $this->set('requests',$req);  
}
public function answeredrequest(){
     $this->set('title_for_layout','list requests')  ;
        
     $req=$this->Requests->find('all',array('conditions'=>array('Status'=>'بررسی شد','user_id'=>$this->Auth->user('id')),'field'=>array('request','status','id')))->all();
     $this->set('requests',$req);
}
public function detail($id){
    
    $request = $this->Requests->get($id, [
            'contain' => ['Users', 'Experts']
        ]);

        $this->set('request', $request);
        $this->set('_serialize', ['request']);
   
}
}
