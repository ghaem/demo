<?php
namespace App\Controller\Admin;

use App\Controller\AppsController;

/**
 * Supercoach Controller
 *
 * @property \App\Model\Table\SupercoachTable $Supercoach
 */
class SupercoachController extends AppsController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $supercoach = $this->paginate($this->Supercoach);

        $this->set(compact('supercoach'));
        $this->set('_serialize', ['supercoach']);
    }

    /**
     * View method
     *
     * @param string|null $id Supercoach id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $supercoach = $this->Supercoach->get($id, [
            'contain' => []
        ]);

        $this->set('supercoach', $supercoach);
        $this->set('_serialize', ['supercoach']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $supercoach = $this->Supercoach->newEntity();
        if ($this->request->is('post')) {
            $supercoach = $this->Supercoach->patchEntity($supercoach, $this->request->getData());
            if ($this->Supercoach->save($supercoach)) {
                $this->Flash->success(__('The supercoach has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The supercoach could not be saved. Please, try again.'));
        }
        $this->set(compact('supercoach'));
        $this->set('_serialize', ['supercoach']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Supercoach id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $supercoach = $this->Supercoach->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $supercoach = $this->Supercoach->patchEntity($supercoach, $this->request->getData());
            if ($this->Supercoach->save($supercoach)) {
                $this->Flash->success(__('The supercoach has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The supercoach could not be saved. Please, try again.'));
        }
        $this->set(compact('supercoach'));
        $this->set('_serialize', ['supercoach']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Supercoach id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $supercoach = $this->Supercoach->get($id);
        if ($this->Supercoach->delete($supercoach)) {
            $this->Flash->success(__('The supercoach has been deleted.'));
        } else {
            $this->Flash->error(__('The supercoach could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
       public function addlogin()
   {
     if ($this->request->is('post')) {
     $supercoach = $this->Auth->identify();
     if ($supercoach) {
     $this->Auth->setUser($supercoach);
     //return $this->redirect($this->Auth->redirectUrl());
     return $this->redirect([
           'controller' => 'supercoachs',
            'action' => 'index']);
     }
     $this->Flash->error('رمز یا ایمیل صحیح نمی باشد');
    }
  }
    public function initialize()
   {
    parent::initialize();
      $this->Auth->allow(['logout']);
    }
    public function addlogout()
    {
       $this->Flash->success('شما خارج شدید');
       return $this->redirect($this->Auth->logout());
    }
}
