<?php
namespace App\Controller\Admin;

use App\Controller\AppsController;

/**
 * Supercoachs Controller
 *
 * @property \App\Model\Table\SupercoachsTable $Supercoachs
 */
class SupercoachsController extends AppsController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $supercoachs = $this->paginate($this->Supercoachs);

        $this->set(compact('supercoachs'));
        $this->set('_serialize', ['supercoachs']);
    }

    /**
     * View method
     *
     * @param string|null $id Supercoach id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $supercoach = $this->Supercoachs->get($id, [
            'contain' => []
        ]);

        $this->set('supercoach', $supercoach);
        $this->set('_serialize', ['supercoach']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $supercoach = $this->Supercoachs->newEntity();
        if ($this->request->is('post')) {
            $supercoach = $this->Supercoachs->patchEntity($supercoach, $this->request->getData());
            if ($this->Supercoachs->save($supercoach)) {
                $this->Flash->success(__('The supercoach has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The supercoach could not be saved. Please, try again.'));
        }
        $this->set(compact('supercoach'));
        $this->set('_serialize', ['supercoach']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Supercoach id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $supercoach = $this->Supercoachs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $supercoach = $this->Supercoachs->patchEntity($supercoach, $this->request->getData());
            if ($this->Supercoachs->save($supercoach)) {
                $this->Flash->success(__('The supercoach has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The supercoach could not be saved. Please, try again.'));
        }
        $this->set(compact('supercoach'));
        $this->set('_serialize', ['supercoach']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Supercoach id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $supercoach = $this->Supercoachs->get($id);
        if ($this->Supercoachs->delete($supercoach)) {
            $this->Flash->success(__('The supercoach has been deleted.'));
        } else {
            $this->Flash->error(__('The supercoach could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
       public function addlogin()
   {
     if ($this->request->is('post')) {
     $supercoach = $this->Auth->identify();
     if ($supercoach) {
     $this->Auth->setUser($supercoach);
     //return $this->redirect($this->Auth->redirectUrl());
     return $this->redirect([
           'controller' => 'Supercoachs',
            'action' => 'index']);
     }
     $this->Flash->error('رمز یا ایمیل صحیح نمی باشد');
    }
  }
    public function initialize()
   {
    parent::initialize();
      $this->Auth->allow(['logout']);
    }
    public function logout()
    {
       $this->Flash->success('شما خارج شدید');
       return $this->redirect($this->Auth->logout());
    }
}
