<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $daraee->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $daraee->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Daraees'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="daraees form large-9 medium-8 columns content">
    <?= $this->Form->create($daraee) ?>
    <fieldset>
        <legend><?= __('Edit Daraee') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('number_daraee');
            echo $this->Form->control('type');
            echo $this->Form->control('client');
            echo $this->Form->control('number_seryal');
            echo $this->Form->control('status');
            echo $this->Form->control('departeman');
            echo $this->Form->control('recever');
            echo $this->Form->control('maker');
            echo $this->Form->control('model');
            echo $this->Form->control('adress_ip');
            echo $this->Form->control('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
