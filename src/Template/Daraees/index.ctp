
    <h3><?= __('Daraees') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('number_daraee') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('client') ?></th>
                <th scope="col"><?= $this->Paginator->sort('number_seryal') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('departeman') ?></th>
                <th scope="col"><?= $this->Paginator->sort('recever') ?></th>
                <th scope="col"><?= $this->Paginator->sort('maker') ?></th>
                <th scope="col"><?= $this->Paginator->sort('model') ?></th>
                <th scope="col"><?= $this->Paginator->sort('adress_ip') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($daraees as $daraee): ?>
            <tr>
                <td><?= $this->Number->format($daraee->id) ?></td>
                <td><?= h($daraee->name) ?></td>
                <td><?= $this->Number->format($daraee->number_daraee) ?></td>
                <td><?= h($daraee->type) ?></td>
                <td><?= h($daraee->client) ?></td>
                <td><?= $this->Number->format($daraee->number_seryal) ?></td>
                <td><?= h($daraee->status) ?></td>
                <td><?= h($daraee->departeman) ?></td>
                <td><?= h($daraee->recever) ?></td>
                <td><?= h($daraee->maker) ?></td>
                <td><?= h($daraee->model) ?></td>
                <td><?= h($daraee->adress_ip) ?></td>
                <td><?= $daraee->has('user') ? $this->Html->link($daraee->user->name, ['controller' => 'Users', 'action' => 'view', $daraee->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $daraee->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $daraee->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $daraee->id], ['confirm' => __('Are you sure you want to delete # {0}?', $daraee->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
                    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>


</div>
