<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Daraee'), ['action' => 'edit', $daraee->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Daraee'), ['action' => 'delete', $daraee->id], ['confirm' => __('Are you sure you want to delete # {0}?', $daraee->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Daraees'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Daraee'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="daraees view large-9 medium-8 columns content">
    <h3><?= h($daraee->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($daraee->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($daraee->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Client') ?></th>
            <td><?= h($daraee->client) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($daraee->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Departeman') ?></th>
            <td><?= h($daraee->departeman) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Recever') ?></th>
            <td><?= h($daraee->recever) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Maker') ?></th>
            <td><?= h($daraee->maker) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Model') ?></th>
            <td><?= h($daraee->model) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Adress Ip') ?></th>
            <td><?= h($daraee->adress_ip) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $daraee->has('user') ? $this->Html->link($daraee->user->name, ['controller' => 'Users', 'action' => 'view', $daraee->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($daraee->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Number Daraee') ?></th>
            <td><?= $this->Number->format($daraee->number_daraee) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Number Seryal') ?></th>
            <td><?= $this->Number->format($daraee->number_seryal) ?></td>
        </tr>
    </table>
</div>
