<nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                        
                          
                               <li> <?= $this ->Html->link('logout',['controller'=>'users','action'=>'logout']); ?></li>
                               <li> <?= $this ->Html->link('request',['controller'=>'requests','action'=>'request']); ?></li>
                               <li> <?= $this ->Html->link('login',['controller'=>'users','action'=>'login']); ?></li>
                               <li> <?= $this ->Html->link('register',['controller'=>'users','action'=>'register']); ?></li>
                                
                                
                             
                             
                         
            </ul>
        </div>
    </nav>
<br>
<div class="index large-4 medium-4 large-offset-4 columns">
    <div class="panel">
        <h2 class="text-center">لطفا درخواست خود را وارد کنید</h2>
           
           <?= $this->Form->create();?>
         <p>* نوع در خواست<select name="Type">
  <option value="email">ایمیل</option>
  <option value="network">شبکه</option>
  <option value="software">نرم افزار</option>
   <option value="etc">غیره</option>
</select> </p>
         
         <p>* درخواست<input type="text" name="request" value=""> </p>
         <p>* اولویت درخواست<select name="Prority">
  <option value="common">عادی</option>
  <option value="important">مهم</option>
  <option value="medium">متوسط</option>
   <option value="immediate">فوری</option>
</select> </p>
          
            <br><br>
           <?= $this->Form->button('ارسال درخواست');?>
           
           
           <?= $this->Form->end();?>
    </div>
</div>

<br>

