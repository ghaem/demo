<h1>
Bookmarks tagged with
<?= $this->Text->toList(h($tags)) ?>
</h1>
<section>
<?php foreach ($requests as $request): ?>
<article>
<!-- Use the HtmlHelper to create a link -->
<h4><?= $this->Html->link($request->title, $request->url) ?></h4>
<small><?= h($request->url) ?></small>
<!-- Use the TextHelper to format text -->
<?= $this->Text->autoParagraph(h($request->description)) ?>
</article>
<?php endforeach; ?>
</section>
