<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Request'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Experts'), ['controller' => 'Experts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Expert'), ['controller' => 'Experts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="requests index large-9 medium-8 columns content">
    <h3><?= __('Requests') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('request') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Prority') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expert_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Numrequest') ?></th>
                <th scope="col"><?= $this->Paginator->sort('response') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($requests as $request): ?>
            <tr>
                <td><?= $this->Number->format($request->Id) ?></td>
                <td><?= h($request->request) ?></td>
                <td><?= h($request->Type) ?></td>
                <td><?= h($request->Created) ?></td>
                <td><?= h($request->Status) ?></td>
                <td><?= h($request->Prority) ?></td>
                <td><?= h($request->Modified) ?></td>
                <td><?= $request->has('user') ? $this->Html->link($request->user->name, ['controller' => 'Users', 'action' => 'view', $request->user->id]) : '' ?></td>
                <td><?= $request->has('expert') ? $this->Html->link($request->expert->Id, ['controller' => 'Experts', 'action' => 'view', $request->expert->Id]) : '' ?></td>
                <td><?= $this->Number->format($request->Numrequest) ?></td>
                <td><?= h($request->response) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $request->Id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $request->Id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $request->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $request->Id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
