<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Knowledge'), ['action' => 'edit', $knowledge->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Knowledge'), ['action' => 'delete', $knowledge->id], ['confirm' => __('Are you sure you want to delete # {0}?', $knowledge->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Knowledges'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Knowledge'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="knowledges view large-9 medium-8 columns content">
    <h3><?= h($knowledge->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Titel') ?></th>
            <td><?= h($knowledge->titel) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Headlines') ?></th>
            <td><?= h($knowledge->Headlines) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Points') ?></th>
            <td><?= h($knowledge->points) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Writer') ?></th>
            <td><?= h($knowledge->writer) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Show User') ?></th>
            <td><?= h($knowledge->Show_user) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($knowledge->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Number') ?></th>
            <td><?= $this->Number->format($knowledge->number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Number People') ?></th>
            <td><?= $this->Number->format($knowledge->Number_people) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Date') ?></th>
            <td><?= h($knowledge->Last_date) ?></td>
        </tr>
    </table>
</div>
