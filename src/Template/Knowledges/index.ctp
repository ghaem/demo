<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Knowledge'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="knowledges index large-9 medium-8 columns content">
    <h3><?= __('Knowledges') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('titel') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Headlines') ?></th>
                <th scope="col"><?= $this->Paginator->sort('points') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Number_people') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Last_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('writer') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Show_user') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($knowledges as $knowledge): ?>
            <tr>
                 <td><?= h($knowledge->name) ?></td>
                 <td><?= $this->Number->format($knowledge->number_daraee) ?></td>
                 <td><?= h($knowledge->type) ?></td>
                 <td><?= h($knowledge->client) ?></td>
                 <td><?= $this->Number->format($knowledge->number_seryal) ?></td>
                <td><?= h($knowledge->status) ?></td>
                <td><?= h($knowledge->departeman) ?></td>
                <td><?= h($knowledge->recever) ?></td>
                <td><?= h($knowledge->maker) ?></td>
                <td><?= h($knowledge>model) ?></td>
                <td><?= h($knowledge->adress_ip) ?></td>
               
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $knowledge->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $knowledge->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $knowledge->id], ['confirm' => __('Are you sure you want to delete # {0}?', $knowledge->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
