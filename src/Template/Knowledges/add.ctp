<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Knowledges'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="knowledges form large-9 medium-8 columns content">
    <?= $this->Form->create($knowledge) ?>
    <fieldset>
        <legend><?= __('Add Knowledge') ?></legend>
        <?php
            echo $this->Form->control('number');
            echo $this->Form->control('titel');
            echo $this->Form->control('Headlines');
            echo $this->Form->control('points');
            echo $this->Form->control('Number_people');
            echo $this->Form->control('Last_date');
            echo $this->Form->control('writer');
            echo $this->Form->control('Show_user');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
