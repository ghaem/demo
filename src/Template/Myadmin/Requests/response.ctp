<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <ul class="side-nav">
       
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $request->Id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $request->Id)]
            )
        ?></li>
        
    </ul> 
</nav>
<div class="requests form large-9 medium-8 columns content">
    <?= $this->Form->create($request) ?>
    <fieldset>
        <legend><?= __('Edit Request') ?></legend>
        <?php
            echo $this->Form->control('request');
            echo $this->Form->control('Type');
            echo $this->Form->control('Created');
            echo $this->Form->control('Status');
            echo $this->Form->control('Prority');
            echo $this->Form->control('Modified');
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('expert_id', ['options' => $experts, 'empty' => true]);
            echo $this->Form->control('Numrequest');
             echo $this->Form->control('password');
              echo $this->Form->control('response');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
