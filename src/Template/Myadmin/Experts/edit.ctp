<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $expert->Id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $expert->Id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Experts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Requests'), ['controller' => 'Requests', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Request'), ['controller' => 'Requests', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="experts form large-9 medium-8 columns content">
    <?= $this->Form->create($expert) ?>
    <fieldset>
        <legend><?= __('Edit Expert') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('Family');
            echo $this->Form->control('Email');
            echo $this->Form->control('Status');
            echo $this->Form->control('Expertise');
            echo $this->Form->control('Education');
            echo $this->Form->control('password');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
