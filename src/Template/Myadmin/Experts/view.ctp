<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Expert'), ['action' => 'edit', $expert->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Expert'), ['action' => 'delete', $expert->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $expert->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Experts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Expert'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Requests'), ['controller' => 'Requests', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Request'), ['controller' => 'Requests', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="experts view large-9 medium-8 columns content">
    <h3><?= h($expert->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($expert->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Family') ?></th>
            <td><?= h($expert->Family) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($expert->Email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($expert->Status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expertise') ?></th>
            <td><?= h($expert->Expertise) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Education') ?></th>
            <td><?= h($expert->Education) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($expert->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($expert->Id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Requests') ?></h4>
        <?php if (!empty($expert->requests)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Sabterequest') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Createdsabt') ?></th>
                <th scope="col"><?= __('Stutus') ?></th>
                <th scope="col"><?= __('Prority') ?></th>
                <th scope="col"><?= __('Createdrequest') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Expert Id') ?></th>
                <th scope="col"><?= __('Numberrequest') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($expert->requests as $requests): ?>
            <tr>
                <td><?= h($requests->Id) ?></td>
                <td><?= h($requests->Sabterequest) ?></td>
                <td><?= h($requests->Type) ?></td>
                <td><?= h($requests->Createdsabt) ?></td>
                <td><?= h($requests->Stutus) ?></td>
                <td><?= h($requests->Prority) ?></td>
                <td><?= h($requests->Createdrequest) ?></td>
                <td><?= h($requests->Modified) ?></td>
                <td><?= h($requests->user_id) ?></td>
                <td><?= h($requests->expert_id) ?></td>
                <td><?= h($requests->Numberrequest) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Requests', 'action' => 'view', $requests->Id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Requests', 'action' => 'edit', $requests->Id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Requests', 'action' => 'delete', $requests->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $requests->Id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
