<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Supercoach'), ['action' => 'edit', $supercoach->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Supercoach'), ['action' => 'delete', $supercoach->id], ['confirm' => __('Are you sure you want to delete # {0}?', $supercoach->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Supercoachs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supercoach'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="supercoachs view large-9 medium-8 columns content">
    <h3><?= h($supercoach->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($supercoach->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($supercoach->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($supercoach->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Family') ?></th>
            <td><?= h($supercoach->family) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($supercoach->id) ?></td>
        </tr>
    </table>
</div>
