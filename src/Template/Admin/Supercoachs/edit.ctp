<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $supercoach->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $supercoach->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Supercoachs'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="supercoachs form large-9 medium-8 columns content">
    <?= $this->Form->create($supercoach) ?>
    <fieldset>
        <legend><?= __('Edit Supercoach') ?></legend>
        <?php
            echo $this->Form->control('username');
            echo $this->Form->control('password');
            echo $this->Form->control('name');
            echo $this->Form->control('family');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
