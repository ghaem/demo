<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Download'), ['action' => 'edit', $download->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Download'), ['action' => 'delete', $download->id], ['confirm' => __('Are you sure you want to delete # {0}?', $download->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Downloads'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Download'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="downloads view large-9 medium-8 columns content">
    <h3><?= h($download->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($download->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Link') ?></th>
            <td><?= h($download->link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Download') ?></th>
            <td><?= h($download->download) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($download->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($download->description)); ?>
    </div>
</div>
